# Copyright 2016 Marc-Antoine Perennou <marc-antoine.perennou@clever-cloud.com>
# Copyright 2016 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="runc container cli tools"
DESCRIPTION="
runc is a CLI tool for spawning and running containers according to the OCI specification.
"
HOMEPAGE="https://www.opencontainers.org/"
BUGS_TO="marc-antoine.perennou@clever-cloud.com"
LICENCES="Apache-2.0"

COMMIT="09c8266bf2fcf9519a651b04ae54c967b9ab86ec"

require bash-completion
require github [ user="opencontainers" rev="${COMMIT}" ]

SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"

# tests need to be run in docker
# Do not strip Go binaries
RESTRICT="test strip"

MYOPTIONS="
    man-pages
    seccomp [[ description = [ Syscall filtering ] ]]
    selinux [[ description = [ selinux process and mount labeling ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.10]
        man-pages? ( dev-golang/go-md2man )
    build+run:
        seccomp? ( sys-libs/libseccomp )
"

GOWORK=src/github.com/opencontainers

src_prepare() {
    default

    edo mkdir -p ${GOWORK}
    edo ln -s ${WORK} ${GOWORK}/${PN}
    edo sed -i "s/^COMMIT :=/COMMIT ?=/" Makefile
}

src_compile() {
    local options=(
        $(optionq seccomp && echo seccomp)
        $(optionq selinux && echo selinux)
    )

    cd ${GOWORK}/runc
    GOPATH="$(pwd)" emake BUILDTAGS="$(echo ${options[@]})" COMMIT="${COMMIT}"

    optionq man-pages && edo man/md2man-all.sh
}

src_install() {
    dobin runc
    optionq man-pages && doman man/man8/*.8
    dobashcompletion contrib/completions/bash/runc
}
